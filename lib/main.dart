import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:lbtracker/blocs/auth_bloc_provider.dart';
import 'package:lbtracker/blocs/user_bloc_provider.dart';
import 'package:lbtracker/gui/pages/add.dart';
import 'package:lbtracker/gui/pages/home.dart';
import 'package:lbtracker/gui/pages/login.dart';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';

void main() => runApp(LBTracker());

class LBTracker extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return UserBlocProvider(
      child: AuthBlocProvider(
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'LB Tracker',
          home: MainScreen(),
          routes: {
            'add': (_)=> AddPage(),
            'login': (_) => LoginPage()
          },
        ),
      ),
    );
  }
}



class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() {
    return _MainScreenState();
  }
}

class _MainScreenState extends State<MainScreen> {
  AuthBloc _bloc;

  @override
  initState() {
    super.initState();
    FlutterNfcReader.onTagDiscovered().listen((onData) {
      SnackBar(content: Text(onData.content));
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _bloc = AuthBlocProvider.of(context);
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _handleCurrentScreen();
  }

  Widget _handleCurrentScreen() {
    return StreamBuilder(
      stream: _bloc.userStream,
      builder: (BuildContext context, AsyncSnapshot<FirebaseUser> snapshot) {
        if (snapshot.hasData) {
          FirebaseUser user = snapshot.data;
          return user != null ? HomePage() : LoginPage();
        } else {
          return LoginPage();
        }
      },
    );
  }
}