import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rxdart/rxdart.dart';
import 'package:lbtracker/providers/firebase_auth_provider.dart';


class AuthBloc {
  final _authProvider = FirebaseAuthProvider();
  final _user = BehaviorSubject<FirebaseUser>();
  final _showProgress = BehaviorSubject<bool>();

    
  FirebaseUser get user => _user.value;

  Observable<FirebaseUser> get userStream => _user.stream;
  Observable<bool> get showProgress => _showProgress.stream;
  Function(bool) get changeShowProgress => _showProgress.sink.add;

  AuthBloc() {
    initUser();
  }


  Future<void> initUser() async {
    FirebaseUser user = await _authProvider.initUser();
    if(user != null){
      _user.sink.add(user);
    }else{
      _user.sink.addError("ඇතුළත් වී නොමැත");
    }

    
  }

  Future<void> signInWithGoogle() async{
    try {
      FirebaseUser user = await _authProvider.signInWithGoogle();
      if(user == null) _user.sink.addError("Google ගිණුමෙන් ඇතුල්විය නොහැක");
      _user.sink.add(user);
    }catch(error){
      print(error);
      _user.sink.addError(error);
    }
  }

  Future<void> signOut() async {
    await _authProvider.signOut();
    initUser();
  }

  Future<Null> signUpWithEmailPassword(String email, String password, String fullName) async {
    try {
      FirebaseUser firebaseUser = await _authProvider.signUpWithEmailPassword(email,password,fullName);

      if(firebaseUser == null) _user.sink.addError("ලියාපදිංචි විය නොහැක");
      _user.sink.add(firebaseUser);
    } catch(error) {
      _user.sink.addError(error);
      print(error);
    }

  }

  Future<Null> signinWithEmailPassword(String email, String password) async {
    try{
      FirebaseUser firebaseUser = await _authProvider.signinWithEmailPassword(email, password);
      if(firebaseUser == null) _user.sink.addError("ලබාදුන් තොරතුරු සමග ඇතුලත් විය නොහැක");
      _user.sink.add(firebaseUser);
    }catch(error) {
      _user.sink.addError(error.message);
      print(error.message);
    }
  }

  Future<Null> resendVerificationEmail(String email) async {
    if (user == null) return;
    try {
      await user.sendEmailVerification();
    }catch(e){
      print(e);
    }
  }

  void dispose() async {
    await _user.drain();
    _user.close();
    await _showProgress.drain();
    _showProgress.close();
  }
}