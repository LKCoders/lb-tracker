
import 'package:flutter/material.dart';

class CCRaisedButton extends StatelessWidget {
  final Widget child;
  final Function onPressed;
  final Color color;

  CCRaisedButton({
    Key key, @required this.child, @required this.onPressed, this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: color == null ? Colors.blue : color,
      textColor: Colors.white,
      onPressed: onPressed,
      child: child
    );
  }
}