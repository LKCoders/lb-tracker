import 'package:flutter/material.dart';
import 'package:lbtracker/model/item.dart';
import 'package:lbtracker/gui/custom_comp/raised_button_comp.dart';


class ItemDetailsForm extends StatelessWidget {
  const ItemDetailsForm({
    Key key,
    @required GlobalKey<FormState> formKey,
    this.item,
    this.category,
    @required this.type,
    @required this.loading,
    @required this.onSubmit,
    @required this.onSaveField,
  }) : _formKey = formKey, super(key: key);

  final GlobalKey<FormState> _formKey;
  final ItemModel item;
  final String category;
  final String type;
  final bool loading;
  final Function onSubmit;
  final Function onSaveField;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: ListView(
        children: <Widget>[
          Text("${item != null ? item.category : category} විස්තර"),
          SizedBox(height: 10.0,),
          TextFormField(
            initialValue: item != null ? item.category == 'මුදල්' ? "${item.amount}" : item.name : "",
            onSaved: (value) {
              onSaveField(
                (item != null ? item.category == "මුදල්" : category == "මුදල්") ? "amount": "name",  (item != null ? item.category == "මුදල්" : category == "මුදල්") ? double.parse(value) :value);
            },
            validator: (value){
              if(value.isEmpty)
                return (item != null ? item.category == "මුදල්" : category == "මුදල්")
                  ? "මුදල් ප්‍රමාණය අවශ්‍යයි"
                  : "භාණ්ඩයේ නම අවශ්‍යයි";
            },
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: (item != null ? item.category == "මුදල්" : category == "මුදල්") ? "මුදල" : "නම"
            ),
          ),
          (item != null ? item.category == "මුදල්" : category == "මුදල්")
            ? SizedBox(height: 10.0,): Container(height: 0,),
          (item != null ? item.category == "මුදල්" : category == "මුදල්")
            ? TextFormField(
              initialValue: item != null ? item.currency : "රු",
              onSaved: (value)=> onSaveField("currency", value),
              validator: (value){
                if(value.isEmpty)
                  return "මුදල් ඒකකය අවශ්‍යයි";
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "ඒකකය"
              ),
            ) : Container(height: 0,),
          SizedBox(height: 10.0,),
          TextFormField(
            initialValue: item != null ? item.note : "",
            onSaved: (value)=> onSaveField("note", value),
            maxLines: 3,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: "වැඩිදුර විස්තර"
            ),
          ),
          SizedBox(height: 30.0,),
          Text(type=='borrowed' ? "ලබාගත්තේ" : "ලබාදුන්නේ"),
          SizedBox(height: 10.0,),
          TextFormField(
            initialValue: item != null ? item.person.name : "",
            onSaved: (value)=> onSaveField("person", {"name":value}),
            validator: (value){
              if(value.isEmpty)
                return "පුද්ගලයාගේ නම අවශ්‍යයි";
            },
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: "නම"
            ),
          ),
          SizedBox(height: 10.0,),
          CCRaisedButton(
            child: Text("සුරකින්න".toUpperCase()),
            onPressed: loading ? null : () => onSubmit(context),
          )
        ],
      ),
    );
  }
}