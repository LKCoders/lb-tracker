import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:lbtracker/blocs/auth_bloc_provider.dart';
import 'package:lbtracker/gui/custom_comp/outline_button_comp.dart';
import 'package:lbtracker/gui/custom_comp/raised_button_comp.dart';
import 'package:lbtracker/gui/custom_comp/text_field_comp.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() {
    return new _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final _formData = {
    "email":"",
    "password":""
  };
  bool _inProgress = false;

  _showErrorDialog({String message = "පරිශීලක නාමය සහ මුරපදය අවශ්‍යයි."}) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("ඇතුල්වීමට නොහැක"),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text("හරි"),
              onPressed: (){
                Navigator.pop(context);
              },
            )
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: SingleChildScrollView(
          padding: EdgeInsets.all(40.0),
          child: Column(
            children: <Widget>[
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 30,),
                    Text("LB Tracker".toUpperCase(), style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 40.0
                      ),
                      textAlign: TextAlign.center,
                    ),
                    StreamBuilder(
                      stream: AuthBlocProvider.of(context).showProgress,
                      builder: (BuildContext context, AsyncSnapshot<bool> snapshot){
                        if(snapshot.hasData && snapshot.data) {
                          return CircularProgressIndicator();
                        }else{
                          return SizedBox(height: 20.0,);
                        }
                      },
                    ),
                    SizedBox(height: 10,),
                    _buildGoogleLogin(),
                    SizedBox(height: 30,),
                    SizedBox(width: double.infinity, child: Text("හෝ ඔබේ ඊමේල් ගිණුම යොදාගන්න", textAlign: TextAlign.left,)),
                    SizedBox(height: 10,),
                    PBTextField(
                      leadingIcon: Icons.alternate_email,
                      hintText: "ඊමේල්",
                      onSaved: (value)=>_formData["email"]=value,
                    ),
                    SizedBox(height: 10.0,),
                    PBTextField(
                      leadingIcon: Icons.vpn_key,
                      hintText: "මුරපදය",
                      onSaved: (value)=>_formData["password"]=value,
                      obscureText: true,              ),
                    SizedBox(height: 5.0,),
                    Container(
                      width: double.infinity,
                      child: GestureDetector(
                        onTap: (){},
                        child: Text("මුරපදය අමතක වුනිද?", textAlign: TextAlign.right, style: TextStyle(
                          color: Colors.black54
                        ),),
                      ),
                    ),
                    SizedBox(height: 20.0,),
                    SizedBox(
                      width: double.infinity,
                      child: CCRaisedButton(
                        child: Text("ඇතුල් වන්න".toUpperCase()),
                        onPressed: _inProgress ? null : () => _handleLogin(context),
                      ),
                    ),
                    
                    SizedBox(height: 40.0,),
                    Text("ගිණුමක් නොමැතිද?"),
                    SizedBox(height: 10.0,),
                    SizedBox(
                      width: double.infinity,
                      child: PBOutlineButton(
                        child: Text("ලියාපදිංචි වන්න".toUpperCase(), style: TextStyle(
                          fontWeight: FontWeight.bold
                        )),
                        onPressed: ()=>Navigator.pushNamed(context, 'signup'),),
                    )

                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildGoogleLogin() {
    return Container(
      width: double.infinity,
      child: CCRaisedButton(
        child: Text(" Google සමග ඉදිරියට"),
        onPressed: _inProgress ? null : () => _handleSocialLogin('google'),
      ),
    );
  }

  _handleLogin(BuildContext context) async {
      AuthBlocProvider.of(context).changeShowProgress(true);
      AuthBlocProvider.of(context).userStream.listen((FirebaseUser user){

      },onError: (error){
        _showErrorDialog(message: error.toString());
      });
      _formKey.currentState.save();
      if(_formData['email'].isEmpty || _formData['password'].isEmpty){
        _showErrorDialog();
      }else{
        await AuthBlocProvider.of(context).signinWithEmailPassword(_formData['email'], _formData['password']);
      }
      AuthBlocProvider.of(context).changeShowProgress(false);
  }

  _handleSocialLogin(String provider) async {
    AuthBlocProvider.of(context).changeShowProgress(true);
    try {
      await AuthBlocProvider.of(context).signInWithGoogle();
    }catch(error){
      print(error);
    }
    AuthBlocProvider.of(context).changeShowProgress(false);
  }

}